diasus



Folders OF_SIM, OF_SINASC and OF_DNDO has ontologies from ontology datasource layer (OF): 

* OF_SINASC.owl, OF_SIM.owl and OF_DNDO.owl ontologies files; 

* OF_SINASC.obda, OF_SIM.obda and OF_DNDO.obda mappings files; 

* OF_SINASC.proprerties, OF_SIM.proprerties and OF_DNDO.proprerties propertires files related to obda files;

* OF_SINASC_Mat.owl, OF_SIM_Mat.owl and OF_DNDO_Mat.owl ontologies test with 10 or 20 materialized invidious;

* OF_SINASC_FR.owl and OF_SIM_FR.owl ontologies examples with British Real Family.


Folder OD_SMI has the ontologies from domain ontology layer (OD):

* OD_SMI.owl - Ontology from mother and infant health;

* OD_SMI.obda and OD_SMI.properties - settings files;

* OD_SMI_FR.owl - Ontology with some British Real Family individuos, just for check;


Folders OI_SINASC, OI_SIM and OI_DNDO has the ontologies from integration ontology layer (OI):

* OI_SINASC.owl, OI_SIM.owl and OI_DNDO.owl ontologies files; 

* OI_SINASC.obda, OI_SIM.obda and OI_DNDO.obda empty mappings files; 

* OI_SINASC.proprerties, OI_SIM.proprerties and OI_DNDO.proprerties empty propertires files related to obda files;
